import React from 'react';
import './Home.css'
const Home = () => {
    return (
        <div className="Home">
          <h1 className="title-home">Home</h1>
            <div className="div-text-home">
                 <p className="text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                     Accusantium alias dicta ducimus ex, inventore ipsam modi perspiciatis quia
                     quibusdam quod! Aut autem inventore laudantium! Eligendi illo iure modi placeat?
                     Adipisci dolorum, molestias necessitatibus odio officiis sint vel?
                 </p>
            </div>
            <div className="items">
                <div className="item">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Accusantium alias dicta ducimus ex, inventore ipsam modi perspiciatis quia
                  quibusdam quod! Aut autem inventore laudantium! Eligendi illo iure modi placeat?
                  </p>
                </div>
                <div className="item">
               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                   Blanditiis nisi sed voluptas? Assumenda eum ipsam labore
                   necessitatibus officia quisquam sit!</p>
                </div>
                <div className="item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus laudantium nostrum sunt voluptates!</p>
                </div>
                <div className="item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Accusantium alias dicta ducimus ex, inventore ipsam modi perspiciatis quia
                        quibusdam quod! Aut autem inventore</p>
                </div>
            </div>
        </div>
    );
};

export default Home;