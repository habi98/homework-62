import React from 'react';
import { NavLink} from "react-router-dom";

import './Menu.css';

const Menu = () => {
    return (
        <header>
            <div className="container clearfix">
                <h2 className="logo-title"><NavLink  className="logo" to="/home">Logo</NavLink></h2>
                <ul className="nav">
                    <li><NavLink  to="/home">Home</NavLink></li>
                    <li><NavLink  to="/about">About us</NavLink></li>
                    <li><NavLink  to="/contacts">Contacts</NavLink></li>
                </ul>
            </div>
        </header>
    );
};

export default Menu;