import React, { Component,Fragment } from 'react';
import Menu from './components/Menu/Menu'
import Home from './components/Home/Home'
import About from './components/About/About'
import Contacts from './components/Contacts/Contacts'
import {BrowserRouter as Router,  Route} from "react-router-dom";


class App extends Component {
  render() {
    return (

      <Router>
        <Fragment>
            <Menu/>
                <Route exact path="/" component={Home}/>
                <Route path="/home" component={Home}/>
                <Route path="/about" component={About}/>
                <Route path="/contacts" component={Contacts}/>
        </Fragment>

      </Router>
    );
  }
}

export default App;
